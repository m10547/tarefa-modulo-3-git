package com.company;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        System.out.println("-----MENU DE OPÇÕES-----");

        int opcao;
        do {


            System.out.println("|  1. Calcular IMC|");
            System.out.println("|  2. Opção 2|");
            System.out.println("|  3. Sair   |");
            System.out.println("  Selecione a opção desejada   ");

            Scanner scanner = new Scanner(System.in);
            opcao = scanner.nextInt();
            processar(opcao);
        } while (opcao != 3);
    }
    public static void processar(int opcao){
        switch (opcao){
            case 1: {
                DecimalFormat deci = new DecimalFormat("0.0");
                // inseri o DecimalFormat para limitar o IMC a um decimal
                Scanner scanner = new Scanner(System.in);
                System.out.println("Calculadora de IMC");

                System.out.println("Insira seu peso (Kg): ");
                float numero1 = scanner.nextFloat();

                System.out.println("Insira sua altura (m): ");
                float numero2 = scanner.nextFloat();
                float imc = numero1 / (numero2 * numero2);

                System.out.println("seu IMC é " + (deci.format(imc)));

                if (imc<18.5){
                    System.out.println("abaixo do peso");
                }

                if (imc >= 18.5 && imc<= 24.9){
                    System.out.println("Peso normal");
                }

                if (imc >= 25 && imc<= 29.9){
                    System.out.println("Sobrepeso");
                }

                if (imc >= 30 && imc<= 34.9){
                    System.out.println("Obesidade Grau I");
                }

                if (imc >= 35 && imc<= 39.9){
                    System.out.println("Obesidade Grau II");
                }

                if (imc >= 40){
                    System.out.println("Obesidade Grau III");
                }

            }
            break;
            case 2: {
                System.out.println("Você escolheu a segunda opção");
            }
            break;

            case 3: {
                System.out.println("O programa foi encerrado");
            }

        }
    }
}
// fazendo push para gitlab
// linha para push